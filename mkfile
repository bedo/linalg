</$objtype/mkfile

LIB=/$objtype/lib/liblinalg.a
OFILES=\
	linalg.$O\

HFILES=/sys/include/linalg.h

UPDATE=\
	mkfile\
	$HFILES\
	${OFILES:%.$O=%.c}\
	${LIB:/$objtype/%=/386/%}\

</sys/src/cmd/mksyslib

